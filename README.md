# Install

`composer require raakrdam/dynamics-crm-bundle` inside your Symfony 4+ project.

# Usage

Developing a composer package needs some trickery.

- First, composer itself doesn't have a feature to mark a package as yours! So with any command fired at composer the 
package is reinstalled, synced, deleted etc. **Beware**
- Composer removes the .git directory by default and therefor it is no source! (there are tricks to do so but I have 
choosen not to use it.

## Solution

Clone the repository outside your project (In my case project: `~/Projects/raakrdam/connekt/lean-and-green`, 
working directory: `~/Projects/raakrdam/symfony/dynamics-crm-bundle`). When you want to do something with composer sync 
the source with `make run-copy-from-project` (works only when your directory setup equals mine, check `Makefile`).

# Implementations

Every Entity (Dynamics) has a lowercase name, eg 'contact', every Doctrine Entity that will sync with Dynamics 
implements the `RaakRdam\DynamicsCRMBundle\Entity\DynamicsInterface`
