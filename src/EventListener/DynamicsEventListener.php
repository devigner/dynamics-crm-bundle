<?php declare(strict_types=1);

namespace RaakRdam\DynamicsCRMBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use RaakRdam\DynamicsCRMBundle\Dynamics\ClientTrait;
use RaakRdam\DynamicsCRMBundle\Entity\DynamicsInterface;

class DynamicsEventListener
{
    use ClientTrait;

    /**
     * @param LifecycleEventArgs $args
     */
    public function prePersist(LifecycleEventArgs $args): void
    {
        $localEntity = $args->getEntity();
        if (!$localEntity instanceof DynamicsInterface) {
            return;
        }

        if (!$localEntity->canSync()) {
            return;
        }

        $this->dynamicsConnector->syncUserWithDynamicsOnChange($localEntity);
    }
}
