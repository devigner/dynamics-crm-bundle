<?php declare(strict_types=1);

namespace RaakRdam\DynamicsCRMBundle\Event;

class DynamicsEvents
{
    public const ENTITY_CREATE_EVENT = 'createEntityEvent';
}
